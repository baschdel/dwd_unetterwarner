#!/bin/bash
curl https://www.dwd.de/DE/wetter/warnungen_gemeinden/warntabellen/warntabellen_node.html | sed 's/<h2/\n|td>>/g' | grep '^|' | tr "<" "\n" | grep 'td>.' | sed 's/td>//g' | cut -f1,3 -d">" | sed 's/|>/>/' | sed 's/&uuml;/ü/g' | sed 's/&auml;/ä/g' | sed 's/&ouml;/ö/g' | sed 's/&Uuml;/Ü/g' | sed 's/&Auml;/Ä/g' | sed 's/&Ouml;/Ö/g' | sed 's/&szlig;/ß/g' | sed 's/&sup2;/²/g'

#FORMAT:
# >LOCATION
# WARNING
# FROM WHEN
# UNTIL WHEN
# DESCRIPTION