#!/bin/bash
warning=$(bash unwetter_format.sh "$1" long 60)
if [ $(echo "$warning" | wc -l) == 1 ]
then
	notify-send "$warning"
else
	notify-send "$(echo "$warning" | head -n1)" "$(echo "$warning" | tail -n +3)"
fi