# Unwetterwarner

This collection contains some scripts to fetch and process weather warnings from https://www.dwd.de/DE/wetter/warnungen_gemeinden/warntabellen/warntabellen_node.html

## What is in here?
- fetch_single.sh <location> | greps trough the page for a location and outputs in a similar format to the unwetter.sh script this was an early prototype
- unwetter.sh | converts the whole page to a series of records (for the exact format look at the comments in unwetter.sh) use ´grep <location> -A4´ to find the warning for <location> (if any)
- unwetter_format.sh &lt;location&gt; [long [&lt;wrap_chars&gt;]] | greps trought the output of unwetter.sh and formats it as a single human readable line or something that can be put into a notification if the wrap_chars argument is specified the long description will be harwarapped using the fold command
- unwetter_notify.sh &lt;location&gt; | uses the output of unwetter_format.sh to display the warning for the specified location using notify-send

## Requirements
- bash
- curl
- sed
- tr
- grep
- cut
- head (only for the format and notify scripts)
- tail (only for the format and notify scripts, notify script requires gnu version)
- fold (only when harwarpping or using the notify script)
- notfy-send (only for the notify script)

## Polybar

I have written a polybar module that checks for updates every 10 minutes and displays them (if any).
Please note, that you have to modify the below configuration to make it work (just use a bit of common sense) but its better than starting from scratch
```
[module/unwetter]
type = custom/script

;I have a script at .scripts/runscript that cds into my script folder and passes whatever it got to bash
exec = bash .scripts/runscript.sh unwetter_format.sh &lt;location&gt;
interval = 600
click-left = bash .scripts/runscript.sh unwetter_notification.sh &lt;location&gt;

format-underline = ${xrdb:color1}
```