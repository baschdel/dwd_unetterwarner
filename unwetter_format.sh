#!/bin/bash
location="$1"

warning=$(bash unwetter.sh 2> /dev/null | grep "$location" -A4)

maxlen=$3

if [ -z "$warning" ]
then
	if [ "$2" == "long" ]
	then
		echo "Keine Unwetterwarnung für $location"
	fi
	exit
fi

format_time() {
	timestamp=$(echo "$1" | sed "s/ Jan/01/" | sed "s/ Feb/02/" | sed "s/ Mar/03/" | sed "s/ Apr/04/" | sed "s/ Mai/05/" | sed "s/ Jun/06/" | sed "s/ Jul/07/" | sed "s/ Aug/08/" | sed "s/ Sep/09/" | sed "s/ Okt/10/" | sed "s/ Nov/11/" | sed "s/ Dez/12/")
}

location=$(echo "$warning" | head -n1 | cut -d">" -f2)
warning_headline=$(echo "$warning" | head -n2 | tail -n1 )
from_date=$(echo "$warning" | head -n3 | tail -n1 | sed s/,//g | cut -d" " -f2,3,4)
until_date=$(echo "$warning" | head -n4 | tail -n1 | sed s/,//g | cut -d" " -f2,3,4)
warning_long=$(echo "$warning" | head -n5 | tail -n1 )

if [ ! -z "$maxlen" ]
then
	warning_long=$(echo "$warning_long" | fold -s -w $maxlen /dev/stdin)
fi

format_time "$from_date"
from_date="$timestamp"
format_time "$until_date"
until_date="$timestamp"

if [ "$2" == "long" ]
then
	echo "$warning_headline für $location"
	echo "$warning_long"
	#echo "┌──────────────────┐"
	#echo "│ von: $from_date │" 
	#echo "│ bis: $until_date │" 
	#echo "└──────────────────┘"
	echo "von: $from_date" 
	echo "bis: $until_date"
else
	echo "$from_date - $until_date | $warning_headline"
fi


